package com.gr.ie.crypto;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import org.bouncycastle.util.encoders.Base64;

/**
 *
 * @author manolis
 */
public class EncryptRSA {

    /**
     *
     * @param text 
     *  String to be encrypted.
     * @param publicKey 
     *  RSA public Key to be used.
     * @return 
     *  Encrypted string.
     */
    public static String getEncrypted(String text, PublicKey publicKey) {
        String encryptedString = null;
        byte[] dataBytes = text.getBytes();
        byte ciphertext[] = null;
        int i = 0;

        System.out.println("\nEncryption RSA BEGIN");
        Cipher rsaCipher;

        try {
            rsaCipher = Cipher.getInstance("RSA/None/PKCS1Padding");

            rsaCipher.init(Cipher.ENCRYPT_MODE, publicKey);

            byte[] tmpText = EncryptAES.addAll(Digest.getHash(Base64.toBase64String(publicKey.getEncoded())), EncryptAES.intToByteArray(text.length()));
            byte[] finalText = EncryptAES.addAll(tmpText, dataBytes);

            ciphertext = rsaCipher.doFinal(finalText);

            encryptedString = Base64.toBase64String(ciphertext);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
            Logger.getLogger(EncryptRSA.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Encryption RSA END\n");
        return encryptedString;
    }

    /**
     *
     * @param text 
     *  String to be encrypted.
     * @param privateKey 
     *  RSA private Key to be used.
     * @return 
     *  Return encrypted string.
     */
    public static String getEncrypted(String text, PrivateKey privateKey) {
        String encryptedString = null;
        byte[] dataBytes = text.getBytes();
        byte ciphertext[] = null;

        System.out.println("\nEncryption RSA BEGIN");
        Cipher rsaCipher;

        try {
            rsaCipher = Cipher.getInstance("RSA/None/PKCS1Padding");

            rsaCipher.init(Cipher.ENCRYPT_MODE, privateKey);

            byte[] tmpText = EncryptAES.addAll(Digest.getHash(Base64.toBase64String(privateKey.getEncoded())), EncryptAES.intToByteArray(text.length()));
            byte[] finalText = EncryptAES.addAll(tmpText, dataBytes);

            ciphertext = rsaCipher.doFinal(finalText);

            encryptedString = Base64.toBase64String(ciphertext);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
            Logger.getLogger(EncryptRSA.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Encryption RSA END\n");
        return encryptedString;
    }
}
