package com.gr.ie.crypto;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

/**
 *
 * @author manolis
 */
public class GenerateAESKey {

    /**
     *
     */
    public GenerateAESKey() {
    }

    /**
     *
     * @return
     *  AES key.
     */
    public static SecretKey getAesKey() {
        SecretKey aesKey = null;
        
        KeyGenerator aesKeygen;
        try {
            aesKeygen = KeyGenerator.getInstance("AES");
            aesKeygen.init(256, new SecureRandom());

            aesKey = aesKeygen.generateKey();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(GenerateAESKey.class.getName()).log(Level.SEVERE,
                    null, ex);
        }
        
        return aesKey;
    }
    
    

}
