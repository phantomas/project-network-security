package com.gr.ie.crypto;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author manolis
 */
public class Digest {

    /**
     *
     */
    public Digest() {
    }

    /**
     * 
     * @param text
     *  Input String.
     * @return 
     *  The hash in bytes.
     */
    public static byte[] getHash(String text) {
        byte[] mdBytes = null;
        String algorithm = "SHA-512";

        try {
            MessageDigest md = MessageDigest.getInstance(algorithm);
            byte[] dataBytes = text.getBytes();

            mdBytes = md.digest(dataBytes);

        } catch (NoSuchAlgorithmException e) {
            System.out.println("Can't create hash");
        }

        return mdBytes;
    }

    /**
     * *
     *
     * @param mdBytes 
     *  Byte array containing the hash.
     * @return 
     *  Hex String containing the hash.
     */
    public static String byteToHexString(byte[] mdBytes) {
        StringBuffer sb;

        sb = new StringBuffer("");
        for (int i = 0; i < mdBytes.length; i++) {
            sb.append(Integer.toString((mdBytes[i] & 0xff) + 0x100, 16).substring(1));
        }

        return sb.toString();
    }
}
