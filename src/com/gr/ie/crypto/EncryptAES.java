package com.gr.ie.crypto;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import org.bouncycastle.util.encoders.Base64;

/**
 *
 * @author manolis
 */
public class EncryptAES {

    /**
     *
     */
    public EncryptAES() {
    }

    /**
     *
     * @param text 
     *  String to be encrypted.
     * @param aesKey 
     *  Key to be used.
     * @return 
     *  Encrypted string.
     */
    public static String getEncryptedString(String text, SecretKey aesKey) {
        String encryptedString = null;
        byte[] dataBytes = text.getBytes();
        byte ciphertext[] = null;

        System.out.println("\nDecryption AES BEGIN");
        Cipher aesCipher;
        try {
            aesCipher = Cipher.getInstance("AES/ECB/PKCS5Padding", "BC");
            aesCipher.init(Cipher.ENCRYPT_MODE, aesKey);

            byte[] tmpText = addAll(Digest.getHash(Base64.toBase64String(aesKey.getEncoded())), intToByteArray(text.length()));
            byte[] finalText = addAll(tmpText, dataBytes);

            ciphertext = aesCipher.doFinal(finalText);

            encryptedString = Base64.toBase64String(ciphertext);
        } catch (NoSuchAlgorithmException | NoSuchProviderException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
            Logger.getLogger(EncryptAES.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Decryption AES END\n");
        return encryptedString;
    }

    /**
     * *
     *
     * @param a 
     *  First byte array to be concatenated.
     * @param b 
     *  Second byte array to be concatenated.
     * @return 
     *  New byte array which is the result of the merge.
     */
    public static byte[] addAll(byte[] a, byte[] b) {
        byte[] c = new byte[a.length + b.length];
        System.arraycopy(a, 0, c, 0, a.length);
        System.arraycopy(b, 0, c, a.length, b.length);

        return c;
    }

    /**
     * *
     *
     * @param value 
     * Integer value to be converted to byte.
     * @return 
     * Value in byte[].
     */
    public static byte[] intToByteArray(int value) {
        return new byte[]{
            (byte) (value >>> 24),
            (byte) (value >>> 16),
            (byte) (value >>> 8),
            (byte) value};
    }

}
