package com.gr.ie.crypto;

import static com.gr.ie.crypto.DecryptAES.byteArraytoInt;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import org.bouncycastle.util.encoders.Base64;

/**
 *
 * @author manolis
 */
public class DecryptRSA {

    /**
     *
     * @param EncryptedText
     *  String to be decrypted.
     * @param privateKey
     *  Key to be used.
     * @return
     *  Decrypted String.
     */
    public static String getdecrypted(String EncryptedText, PrivateKey privateKey) {
        String text = null;
        
        System.out.println("\nDecryption RSA BEGIN");
        try {
            Cipher rsaCipher = Cipher.getInstance("RSA/None/PKCS1Padding");

            rsaCipher.init(Cipher.DECRYPT_MODE, privateKey);

            byte[] raw = Base64.decode(EncryptedText);
            byte[] stringBytes = rsaCipher.doFinal(raw);

            byte[] hashBytes = Arrays.copyOfRange(stringBytes, 0, 64);
            System.out.println("Digest(in hex format):: " + Digest.byteToHexString(hashBytes));

            byte[] sizeBytes = Arrays.copyOfRange(stringBytes, 64, 68);
            int sizeOfText = byteArraytoInt(sizeBytes);
            System.out.println("Message size is " + sizeOfText + " bytes.");

            byte[] plainText = Arrays.copyOfRange(stringBytes, 68, (68 + sizeOfText));
            text = new String(plainText, "UTF8");
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | UnsupportedEncodingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
            Logger.getLogger(DecryptAES.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Decryption RSA END\n");
        return text;
    }

    /**
     *
     * @param EncryptedText
     *  String to be decrypted.
     * @param publicKey
     *  Key to be used.
     * @return
     *  Decrypted String.
     */
    public static String getdecrypted(String EncryptedText, PublicKey publicKey) {
        String text = null;

        System.out.println("\nDecryption RSA BEGIN");
        try {
            Cipher rsaCipher = Cipher.getInstance("RSA/None/PKCS1Padding");

            rsaCipher.init(Cipher.DECRYPT_MODE, publicKey);

            byte[] raw = Base64.decode(EncryptedText);
            byte[] stringBytes = rsaCipher.doFinal(raw);

            byte[] hashBytes = Arrays.copyOfRange(stringBytes, 0, 64);
            System.out.println("Digest(in hex format):: " + Digest.byteToHexString(hashBytes));

            byte[] sizeBytes = Arrays.copyOfRange(stringBytes, 64, 68);
            int sizeOfText = byteArraytoInt(sizeBytes);
            System.out.println("Message size is " + sizeOfText + " bytes.");

            byte[] plainText = Arrays.copyOfRange(stringBytes, 68, (68 + sizeOfText));
            text = new String(plainText, "UTF8");
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | UnsupportedEncodingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
            Logger.getLogger(DecryptAES.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Decryption RSA END\n");
        return text;
    }
}
