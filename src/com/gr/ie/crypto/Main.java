package com.gr.ie.crypto;

import java.io.File;
import java.security.KeyPair;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import org.bouncycastle.util.encoders.Base64;

public class Main {

    private static Digest digest;
    private static String text = "O karvounas ton pernei"; /*with much love <3*/

    public static void main(String[] args) {
        if (args.length > 0) {
            File file = new File(args[0]);
            SecretKey aesKey = GenerateAESKey.getAesKey();
            KeyPair keyPair = GenerateRSAKeyPair.getKeyPair();

            byte[] hashbytes = Digest.getHash(text);
            
            String encryptedStringRSA = EncryptRSA.getEncrypted(Base64.toBase64String(aesKey.getEncoded()), keyPair.getPublic());
            
            String decrStringTextRSA = DecryptRSA.getdecrypted(encryptedStringRSA, keyPair.getPrivate());
            
            byte[] decoded = Base64.decode(decrStringTextRSA);
            SecretKey kgen = new SecretKeySpec(decoded, 0, decoded.length, "AES");
            String encryptedStringAES = EncryptAES.getEncryptedString(text, kgen);

            String decryptedText = DecryptAES.getdecryptedAES(encryptedStringAES, kgen);
            
        }
    }
}
