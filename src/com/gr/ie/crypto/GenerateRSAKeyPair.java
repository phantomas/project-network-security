package com.gr.ie.crypto;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 *
 * @author manolis
 */
public class GenerateRSAKeyPair {

    /**
     *
     */
    public GenerateRSAKeyPair() {
    }

    /**
     *
     * @return
     *  RSA keypair.
     */
    public static KeyPair getKeyPair() {
        KeyPair rsaKeyPair = null;

        try {
            KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
            
            kpg.initialize(2048, new SecureRandom());
            
            rsaKeyPair = kpg.generateKeyPair();
            
        } catch (NoSuchAlgorithmException e) {
            System.err.println("Algorithm not supported! " + e.getMessage() + "!");
        }
        return rsaKeyPair;
    }
}
