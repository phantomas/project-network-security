package com.gr.ie.crypto;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import org.bouncycastle.util.encoders.Base64;

/**
 *
 * @author manolis
 */
public class DecryptAES {

    /**
     *
     */
    public DecryptAES() {
    }

    /**
     *
     * @param EncryptedText 
     *  String to be decrypted.
     * @param aesKey
     *  Key to be used.
     * @return 
     *  Decrypted String.
     */
    public static String getdecryptedAES(String EncryptedText, SecretKey aesKey) {
        String text = null;
        String keyHash = null;

        System.out.println("\nDecryption AES BEGIN");
        try {
            Cipher aesCipher = Cipher.getInstance("AES/ECB/PKCS5Padding", "BC");

            aesCipher.init(Cipher.DECRYPT_MODE, aesKey);

            byte[] raw = Base64.decode(EncryptedText);
            byte[] stringBytes = aesCipher.doFinal(raw);

            byte[] hashBytes = Arrays.copyOfRange(stringBytes, 0, 64);
            keyHash = Digest.byteToHexString(hashBytes);
            System.out.println("Digest(in hex format):: " + keyHash);

            if (!keyHash.equals(Digest.byteToHexString(Digest.getHash(Base64.toBase64String(aesKey.getEncoded()))))) {
                System.out.println("Keys do not match");
                return null;
            }

            byte[] sizeBytes = Arrays.copyOfRange(stringBytes, 64, 68);
            int sizeOfText = byteArraytoInt(sizeBytes);
            System.out.println("Message size is " + sizeOfText + " bytes.");

            byte[] plainText = Arrays.copyOfRange(stringBytes, 68, (68 + sizeOfText));
            text = new String(plainText, "UTF8");
//            System.out.println(text);
//            System.out.println("Decryption Done.");

        } catch (NoSuchAlgorithmException | NoSuchProviderException | NoSuchPaddingException | UnsupportedEncodingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
            Logger.getLogger(DecryptAES.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Decryption AES END\n");
        return text;
    }

    /**
     * *
     *
     * @param a 
     *  Byte array to be converted to int.
     * @return 
     *  The value to the byte array as an int.
     */
    public static int byteArraytoInt(byte[] a) {
        return new Integer(a[0] << 24) & 0xff000000
                | (a[1] << 16) & 0x00ff0000
                | (a[2] << 8) & 0x0000ff00
                | (a[3] << 0) & 0x000000ff;
    }
}
