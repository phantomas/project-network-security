package com.gr.ie.network;

import com.gr.ie.crypto.DecryptAES;
import com.gr.ie.crypto.DecryptRSA;
import com.gr.ie.crypto.EncryptAES;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bouncycastle.util.encoders.Base64;
import com.gr.ie.crypto.GenerateRSAKeyPair;
import java.security.KeyPair;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author manolis
 */
public class Client {

    private String hostname;
    private int port;
    private Socket socketClient;
    private KeyPair rsaKeyPair;
    private SecretKey aesKey;
    private PublicKey ServerPkey;

    /**
     * 
     * @param hostname
     *  Hostname to be connected.
     * @param port 
     *  Port to use.
     */
    public Client(String hostname, int port) {
        this.hostname = hostname;
        this.port = port;
    }

    /**
     * Initialize RSA keypair.
     */
    public void initialize() {
        System.out.println("Initializing keys");
        rsaKeyPair = GenerateRSAKeyPair.getKeyPair();
    }

    /**
     * 
     */
    public void start() {
        initialize();
        try {
            String message;

//          step 1
            connect();
            sendMessage(Base64.toBase64String(rsaKeyPair.getPublic().getEncoded()));
            message = readResponse();
            System.out.println("Server's public key is " + message);

            byte[] keyBytes = Base64.decode(message);

            try {
                X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
                KeyFactory keyFactory = KeyFactory.getInstance("RSA");
                ServerPkey = keyFactory.generatePublic(keySpec);

            } catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
                Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
            }

//          step 2
            connect();
            String msgPart1, msgPart2;
            msgPart1 = readResponse();
            sendMessage("PART01OK");

//          step 3
            connect();
            msgPart2 = readResponse();
            sendMessage("PART02OK");

            msgPart1 = DecryptRSA.getdecrypted(msgPart1, rsaKeyPair.getPrivate());
            msgPart2 = DecryptRSA.getdecrypted(msgPart2, rsaKeyPair.getPrivate());
            message = msgPart1 + msgPart2;
            message = DecryptRSA.getdecrypted(message, ServerPkey);
            System.out.println("Recieved AESKEY from server: " + message);
            byte[] decoded = Base64.decode(message);
            SecretKey kgen = new SecretKeySpec(decoded, 0, decoded.length, "AES");
            aesKey = kgen;

//          step 4
            connect();
            message = "This message is sent through a secure connection";
            message = EncryptAES.getEncryptedString(message, aesKey);
            sendMessage(message);
            message = readResponse();
            message = DecryptAES.getdecryptedAES(message, aesKey);
            System.out.println("\nGot: " + message + " from the Server");

//         
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Connect to Server.
     * @throws UnknownHostException
     * @throws IOException
     */
    public void connect() throws UnknownHostException, IOException {
//        System.out.println("Attempting to connect to " + hostname + ":" + port);
        socketClient = new Socket(hostname, port);
//        System.out.println("Connection Established");
    }

    /**
     *
     * @param message
     *  String to be sent.
     */
    public void sendMessage(String message) {
        OutputStream outstream;
        try {
            outstream = socketClient.getOutputStream();
            PrintWriter out = new PrintWriter(outstream);

            out.print(message);
            out.flush();
            socketClient.shutdownOutput();
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @return
     *  String Received.
     */
    public String readResponse() {
        BufferedInputStream is;
        InputStreamReader isr;
        StringBuffer instr = new StringBuffer();
        try {
            is = new BufferedInputStream(socketClient.getInputStream());
            isr = new InputStreamReader(is);

            int c;
            while ((c = isr.read()) != -1) {
                instr.append((char) c);
            }
            socketClient.shutdownInput();

        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
        return instr.toString();
    }

    /**
     * Client entry point.
     * @param arg
     */
    public static void main(String arg[]) {
        //Creating a SocketClient object    
        Client client = new Client("localhost", 9990);
        client.start();
    }
}
