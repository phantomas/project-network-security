package com.gr.ie.network;

import com.gr.ie.crypto.DecryptAES;
import com.gr.ie.crypto.DecryptRSA;
import com.gr.ie.crypto.EncryptAES;
import com.gr.ie.crypto.EncryptRSA;
import com.gr.ie.crypto.GenerateAESKey;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.KeyPair;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.gr.ie.crypto.GenerateRSAKeyPair;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import org.bouncycastle.util.encoders.Base64;

/**
 *
 * @author manolis
 */
public class Server {

    private ServerSocket serverSocket;
    private Socket client;
    private int port;
    private KeyPair rsaKeyPair;
    private SecretKey aesKey;
    private PublicKey ClientPkey;

    /**
     *
     * @param port Port to listen to.
     */
    public Server(int port) {
        this.port = port;
    }

    /**
     * Initialize RSA keypair and AES key.
     */
    public void initialize() {
        System.out.println("Initializing keys");
        aesKey = GenerateAESKey.getAesKey();
        rsaKeyPair = GenerateRSAKeyPair.getKeyPair();
    }

    /**
     * Start listening for clients.
     */
    public void listen() {
//        System.out.println("Waiting for clients...");
        try {
            client = serverSocket.accept();
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
//        System.out.println("Connection Established");
    }

    /**
     *
     */
    public void start() {
        initialize();
        System.out.println("Starting the socket server at port:" + port);
        try {
            serverSocket = new ServerSocket(port);
            String message;
            String msgPart1, msgPart2;
            boolean safeConnection = false;

            //step 1
            listen();
            message = readResponse(client);
            System.out.println("Client's public key is " + message);
            byte[] keyBytes = Base64.decode(message);
            try {
                X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
                KeyFactory keyFactory = KeyFactory.getInstance("RSA");
                ClientPkey = keyFactory.generatePublic(keySpec);
            } catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
                Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
            }

            sendMessage(client, Base64.toBase64String(rsaKeyPair.getPublic().getEncoded()));

            // step 2
            message = Base64.toBase64String(aesKey.getEncoded());
            System.out.println("Sending AESKEY: " + message);
            message = EncryptRSA.getEncrypted(message, rsaKeyPair.getPrivate());

            msgPart1 = message.substring(0, message.length() / 2);
            msgPart2 = message.substring((message.length() / 2), message.length());

            listen();
            msgPart1 = EncryptRSA.getEncrypted(msgPart1, ClientPkey);
            System.out.println("Sending Part 01");
            sendMessage(client, msgPart1);
            message = readResponse(client);
            System.out.println("Got " + message + " from client");

            // step 3
            listen();
            msgPart2 = EncryptRSA.getEncrypted(msgPart2, ClientPkey);
            System.out.println("Sending Part 02");
            sendMessage(client, msgPart2);
            message = readResponse(client);
            System.out.println("Got " + message + " from client");

            // step 4
            listen();
            message = readResponse(client);
            message = DecryptAES.getdecryptedAES(message, aesKey);
            System.out.println("\nGot: " + message + " from the Client");
            message = "This reply is sent through a secure connection";
            message = EncryptAES.getEncryptedString(message, aesKey);
            sendMessage(client, message);
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     *
     * @param message String to be sent.
     */
    private void sendMessage(Socket client, String message) {
        OutputStream outstream;
        try {
            outstream = client.getOutputStream();
            PrintWriter out = new PrintWriter(outstream);

            out.print(message);
            out.flush();
            client.shutdownOutput();
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     *
     * @return String Received.
     */
    private String readResponse(Socket client) {
        BufferedInputStream is;
        InputStreamReader isr;
        StringBuffer instr = new StringBuffer();
        try {
            is = new BufferedInputStream(client.getInputStream());
            isr = new InputStreamReader(is);

            int c;
            while ((c = isr.read()) != -1) {
                instr.append((char) c);
            }
            client.shutdownInput();
//            is.close();
//            isr.close();

        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
        return instr.toString();
    }

    /**
     * Server entry point.
     * @param args
     */
    public static void main(String[] args) {
        // Setting a default port number.
        int portNumber = 9990;

        Server server = new Server(portNumber);
        server.start();
    }
}
